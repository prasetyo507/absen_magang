<?php 

	function time_ago($datetime, $full = false)
    {
        $datediff = abs($datetime);
        $difftext = "";
        
        $hours = floor($datediff / 3600);
        $mnt = floor($datediff % 3600 / 60);
        $minutes = floor($datediff / 60);
        $dtk = floor($datediff % 60);
        $seconds = floor($datediff);
       
        //hour checker
        if ($difftext == "") {
            if ($hours >= 1)
                $difftext = $hours . " jam".', '.$mnt . " menit".', '.$dtk . " detik";
        }
        //minutes checker
        if ($difftext == "") {
            if ($minutes >= 1)
                $difftext = $minutes . " menit".', '.$dtk . " detik";
        }
        //seconds checker
        if ($difftext == "") {
            if ($seconds >= 1)
                $difftext = $seconds . " detik";
        }
        return $difftext;
    }
	
	function getName($u)
	{
		$CI = &get_instance();
		$usr = $CI->db->where('user_id', $u)->get('tbl_user')->row();
            $nav_name = $usr->nama;
        return $nav_name;
	}
	function getStatus($u)
	{
		$CI = &get_instance();
		$st = $CI->db->where('id_status', $u)->get('tbl_status')->row();
            $status = $st->state;
        return $status;
	}
	function getLevel($u)
	{
		$CI = &get_instance();
		$st = $CI->db->where('id_level', $u)->get('tbl_level')->row();
            $status = $st->privilege;
        return $status;
	}

	function datestrip($date)
	{
		$tahun = substr($date,0,4);
		$bulan = substr($date,4,2);
		$hari = substr($date,6,2);
		return $tahun . '-' . $bulan . '-' . $hari;
	}
	function dateIdn($date)
	{
		$BulanIndo = array("","Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 	$split = explode('-', $date);
		return $split[2] . ' ' . $BulanIndo[ (int)$split[1] ] . ' ' . $split[0];
	}
	function datenoSpace($date)
	{
		$BulanIndo = array("","Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 	$split = explode('-', $date);
		return $split[0] . '' . $split[1] . '' . $split[2];
	}
	function datetimeIdn($date)
	{
		$BulanIndo = array("","Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 	$split = explode(' ', $date);
	 	$spli = explode('-', $split[0]);
		return  $split[1] . ' , ' . $spli[2] . ' ' . $BulanIndo[ (int)$spli[1] ] . ' ' . $spli[0];
	}

	function dateRmw($date)
	{
		$BulanIndo = array("","I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
	 	$split = explode('-', $date);
		return $split[2] . ' ' . $BulanIndo[ (int)$split[1] ] . ' ' . $split[0];
	}
