<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('user_id') == null) {
			$this->session->set_flashdata('error', 'Maaf, Silahkan Login terlebih dahulu');
			redirect('auth');
		}
	}

	public function index()
	{
		$data['user'] = $this->model_crud->getDetail('tbl_user','user_id',$this->session->userdata('user_id'))->row();
		$data['page'] = 'profile/v_profile';
		$this->load->view('template/template', $data);
	}
	function upload()
	{
        $this->load->view('image_upload');
	}
	
	public function create()
	{

		$this->load->helper('inflector');
		$this->load->helper('security');
		$this->load->library('secure_library');
		
		$filenames= date('Ymd_His').'_'.$_FILES['files']['name'];
		
		$filename= str_replace(' ','_',$filenames);

		$config['upload_path']   = './unggah/user/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name']     = $filename;
		$config['max_size']      = '102400';
		$config['max_width']     = '102400';
		$config['max_height']    = '102400';

		$this->load->library('upload', $config);
		/* echo '<pre>';
		print_r($filename);exit(); */
		if (!$this->upload->do_upload('files')) {
			echo '<pre>';
			print_r($this->upload->display_errors());
			echo '<br><a style="color:blue" onclick="history.go(-1)"> << Kembali </a>'; exit();
		} else {

			$this->secure_library->filter_post($_FILES['files']['name'], 'xss_clean');
		/* echo '<pre>';
		print_r($aranfile);exit(); */
			if ($this->secure_library->start_post()) {

				$data = array(
					'foto' 		=> xss_clean($filename)
				);
				
				$row = $this->db->where('user_id',$this->session->userdata('user_id'))->get('tbl_user')->row();
				if ($row->foto!='user.png'){
					unlink('./unggah/user/'.$row->foto);
				}
				$this->model_crud->updatedata('tbl_user','user_id',$this->session->userdata('user_id'), $data);
				echo "<script>alert('Sukses');history.go(-1);</script>";

			} else {

				echo 'Pengunggahan mengalami kendala. Mohon Cek kembali file anda. <a style="color:blue" onclick="history.go(-1)"> << Kembali </a>'; exit();

			}
			
		}
	}
	
	function edit()
	{
		$data = array(
			'nama'			=> $this->input->post('nama'),
			'password'		=> $this->input->post('password')
			);
		//var_dump($data);exit();
		$this->model_crud->updatedata('tbl_user','user_id',$this->session->userdata('user_id'),$data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."profile/';</script>";
	}
	


}
