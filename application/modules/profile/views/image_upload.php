<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div><?php echo validation_errors()?></div>
<form  id="invs" style="margin-left:40px;" class ="form-horizontal" action="<?php echo site_url();?>profile/create" method="post" enctype="multipart/form-data">
    <div class="modal-body">  
        <div class="form-group">
			<label>Upload</label>
			<input type="file" name="files" class="form-control-file">
		<small id="fileHelp" class="form-text text-muted">Gunakan File dengan format .jpeg, .gif atau .png</small>
		</div>
    </div> 
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <input type="submit" class="btn btn-primary" value="Simpan" id="save"/>
    </div>
</form>