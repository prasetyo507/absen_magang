﻿<style>
  .container {
    position: relative;
    width: 100%;
    max-width: 400px;
  }

  /* Make the image to responsive */
  .image {
    width: 100%;
    height: auto;
  }

  /* The overlay effect (full height and width) - lays on top of the container and over the image */
  .overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .3s ease;
    background-color: grey;
  }

  /* When you mouse over the container, fade in the overlay icon*/
  .container:hover .overlay {
    opacity: 0.5;
  }

  /* The icon inside the overlay is positioned in the middle vertically and horizontally */
  .icon {
    color: white;
    font-size: 80px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
  }

  /* When you move the mouse over the icon, change color */
  .mdi-camera:hover {
    color: #ddd;
  }
</style>
<script type="text/javascript">
  function image_upl(id) {
    $("#upload").load('<?php echo base_url() ?>profile/upload/' + id);
  }
</script>
<script>
  function cekPass() {
    var password = document.getElementById('password');
    var konfpass = document.getElementById('konfpass');
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    if (password.value == konfpass.value) {
      konfpass.style.backgroundColor = goodColor;
      message.style.color = goodColor;
      document.getElementById('save').disabled = false;
      message.innerHTML = "Passwords Match!"
    } else {
      konfpass.style.backgroundColor = badColor;
      message.style.color = badColor;
      document.getElementById('save').disabled = true;
      message.innerHTML = "Passwords Do Not Match!"
    }
  }
</script>
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-account-settings-variant"></i>
    </span>
    Profile
  </h3>
</div>
<div class="row">
</div>
<div class="row">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Log Transaksi</h4>
        <form id="inv" class='form-horizontal' action="<?php echo site_url(); ?>profile/edit" method="post" enctype="multipart/form-data">
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-4">
                <div class="container">
                  <img src="<?php echo base_url() . "unggah/user/" . $user->foto; ?>" class="image">
                  <div class="overlay">
                    <a onclick="image_upl(<?php echo $user->user_id; ?>)" data-toggle="modal" href="#uploadModal" class="icon" title="Image">
                      <i class="mdi mdi-camera"></i>
                    </a>
                  </div>
                </div>
              </div>

              <div class="col-lg-8">
								<div class="form-group row" id="">
									<label class="col-sm-3  col-form-label">User ID</label>
									<div class="col-md-9">
										<input type="text" id="userid" name="userid" placeholder="Input UserID" class="form-control" value="<?php echo $user->user_id; ?>" disabled />
										<div id="msg"></div>
									</div>
								</div>
								<div class="form-group row" id="">
									<label class="col-sm-3  col-form-label">Nama</label>
									<div class="col-md-9">
										<input type="text" id="username" name="nama" placeholder="Input Nama" class="form-control" value="<?php echo $user->nama; ?>" required />
									</div>
								</div>
								<div class="form-group row" id="">
									<label class="col-sm-3  col-form-label">Password</label>
									<div class="col-md-9">
										<input type="password" name="password" id="password" class="form-control" placeholder="Input Password" value="<?php echo $user->password; ?>" required>
									</div>
								</div>
								<div class="form-group row" id="">
									<label class="col-sm-3  col-form-label">Konfirmasi Password</label>
									<div class="col-md-9">
										<input type="password" onkeyup="cekPass(); return false;" name="konfpass" class="form-control" id="konfpass" placeholder="Input Konfirmasi Password" required>
										<span id="confirmMessage" class="confirmMessage"></span>
									</div>
								</div>
								<br>
								<div class="form-group">
									<div class="col-sm-12">
										<input id="save" type="submit" class="btn btn-rounded btn-gradient-primary btn-block" value="Simpan" />
									</div>
								</div>
							</div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- edit modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-sm" id="upload">

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>