<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_profile extends CI_Model
{

	function cekAbsen($id)
	{
		$this->db->where('user', $id);
		$this->db->from('tbl_log');
		$this->db->join('tbl_status', 'tbl_log.status = tbl_status.id_status');
		$this->db->order_by('tbl_log.id', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query;
	}

	function ambil($id)
	{
		$this->db->where('user', $id);
		$this->db->from('tbl_log');
		$this->db->join('tbl_status', 'tbl_log.status = tbl_status.id_status');
		$this->db->order_by('tbl_log.id', 'asc');
		$query = $this->db->get();
		return $query;
	}
	function user_magang()
	{
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('level !=', 1);
		$this->db->order_by('nama', 'asc');
		$query = $this->db->get();
		return $query;
	}

	public function print_report($date1, $date2, $nama)
	{
		$condition = "log BETWEEN " . "'" . $date1. "'" . " AND " . "'" . $date2 . "'";
		$this->db->select('*');
		$this->db->from('tbl_log');
		$this->db->where($condition);
		$this->db->where('user', $nama);
		$query = $this->db->get();
			return $query->result();
		
	}
}
/* End of file Crud_model.php */
/* Location: ./application/models/Crud_model.php */
