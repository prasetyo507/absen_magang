<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MX_Controller
{

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if (empty($this->session->userdata('user_id'))) {
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('auth/v_login');
			} else {
				$this->load->model('auth/Model_login');
				$valid_user = $this->Model_login->check_credential();

				if ($valid_user == FALSE) {
					$this->session->set_flashdata('error', 'Username/Password salah!');
					redirect('auth');
				} else {
					$this->session->set_userdata('user_id', $valid_user->user_id);
					$this->session->set_userdata('nama', $valid_user->nama);
					$this->session->set_userdata('level', $valid_user->level);
					$this->session->set_userdata('foto', $valid_user->foto);
					redirect(base_url());
				}
			}
		}else{
			redirect(base_url());
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('auth');
	}
}
