<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('user_id') == null) {
			$this->session->set_flashdata('error', 'Maaf, Silahkan Login terlebih dahulu');
			redirect('auth');
		}
		if ($this->session->userdata('level') != 1) {
			$this->session->set_flashdata('error', 'Maaf, Anda tidak mempusnyai akses');
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->model('user/model_user');
		$data['users'] = $this->model_user->user_magang()->result();
		$data['page'] = 'user/v_user';
		$this->load->view('template/template', $data);
	}
	
	function add()
	{
		$data = array(
			'user_id'		=> $this->input->post('user_id'),
			'nama'			=> $this->input->post('nama'),
			'password'		=> 'Ubharaj4y4',
			'level'			=> 2,
			'foto'			=> 'user.png'
			);
		//var_dump($data);exit();
		$this->model_crud->insertdata('tbl_user',$data);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."user/';</script>";
	}
	
	function delete($id){
		$this->model_crud->deldata('tbl_user','user_id',$id);
		echo "<script>alert('Berhasil');
		document.location.href='".base_url()."user';</script>";
	}
}
