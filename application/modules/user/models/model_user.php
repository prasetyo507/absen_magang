<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_user extends CI_Model
{

	function user_magang()
	{
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('level !=', 1);
		$this->db->order_by('nama', 'asc');
		$query = $this->db->get();
		return $query;
	}
}
/* End of file Crud_model.php */
/* Location: ./application/models/Crud_model.php */
