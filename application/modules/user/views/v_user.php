﻿<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account"></i>
		</span>
		Pengguna
		<a class="btn btn-rounded btn-sm btn-gradient-primary text-white" data-toggle="modal" href="#myModal" ><i class="mdi mdi-account-plus"> Tambah</i></a>
	</h3>
</div>
<div class="row">
</div>
<div class="row">
	<div class="col-12 grid-margin">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Magang</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>UserID</th>
								<th>Nama</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($users as $row) { ?>
								<tr>
									<td><?php echo $row->user_id ?></td>
									<td><?php echo $row->nama ?></td>
									<td>
										<a class="btn btn-danger btn-rounded btn-xs" href="<?php echo base_url('user/delete/').$row->user_id; ?>" onclick="return confirmhapus();" ><i class="mdi mdi-account-minus"> Hapus</i></button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                
            </div>
            <form class ='form-horizontal' action="<?php echo site_url();?>user/add" method="post" enctype="multipart/form-data">
                <div class="modal-body">  
                    <div class="form-group row" id="">
						<label class="col-sm-2  col-form-label">UserID</label>
						<div class="col-md-10">
							<input type="text" id="username" name="user_id" placeholder="Input UserID" class="form-control" value="" required>
						</div>
					</div>
					<div class="form-group row" id="">
						<label class="col-sm-2  col-form-label">Nama</label>
						<div class="col-md-10">
							<input type="text" name="nama" id="password" class="form-control" placeholder="Input Nama" value="" required>
						</div>
					</div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <input type="submit" class="btn btn-primary" value="Simpan" id="save"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->