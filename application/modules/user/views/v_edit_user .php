﻿<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account"></i>
		</span>
		Pengguna
		<a class="btn btn-rounded btn-sm btn-gradient-primary text-white" href="<?php echo base_url('user/tambah') ?>"><i class="mdi mdi-account-plus"> Tambah</i></a>
	</h3>
</div>
<div class="row">
</div>
<div class="row">
	<div class="col-12 grid-margin">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Magang</h4>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>UserID</th>
								<th>Nama</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($users as $row) { ?>
								<tr>
									<td><?php echo $row->user_id ?></td>
									<td><?php echo $row->nama ?></td>
									<td>
										<button class="btn btn-gradient-warning btn-sm" title="Ubah" ><i class="mdi mdi-account-convert"></i></button>
										<button class="btn btn-gradient-danger btn-sm" title="Hapus" ><i class="mdi mdi-account-minus"></i></button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- edit modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-sm" id="upload">

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>