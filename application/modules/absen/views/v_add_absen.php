﻿<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-crosshairs-gps"></i>
    </span>
    Tambah Absensi
  </h3>
</div>
<div class="row">
</div>
<div class="row">
  <div class="col-6 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title"><?php
        if(!empty($cek)){

          if ($cek->state == 'Keluar') {
            echo 'Masuk';
          } else {
            echo 'Keluar';
          }
        } else {
          echo 'Masuk';
        }
          ?></h4>
        <form class='form-horizontal' action="<?php echo site_url(); ?>absen/add" method="post" enctype="multipart/form-data">
          <div class="modal-footer">
            <input type="submit" class="btn btn-rounded btn-lg btn-gradient-success text-white" value="Tap !" />
          </div>
        </form>
      </div>
    </div>
  </div>
</div>