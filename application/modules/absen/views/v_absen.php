﻿<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-crosshairs-gps"></i>
    </span>
    Absensi
    <a class="btn btn-rounded btn-sm btn-gradient-primary text-white" href="<?php echo base_url('absen/tambah') ?>"><i class="mdi mdi-plus-circle"> Absen</i></a>
  </h3>
</div>
<div class="row">
</div>
<div class="row">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Log Transaksi</h4>
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Log</th>
                <th>Status</th>
                <th>Selisih</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($absennya as $row) { ?>
                <tr>
                  <td><?php echo $row->log ?></td>
                  <td><?php echo $row->state ?></td>
                  <?php ?>
                  <td><?php
                      if ($row->selisih == 0) {
                        echo '-';
                      } else {
                        echo time_ago($row->selisih);
                      } ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Laporan</h4>
        <p class="card-description">
          Absensi magang
        </p>
        <form class="forms-sample" action="<?php echo site_url(); ?>absen/print" method="post" enctype="multipart/form-data">
          <?php if ($this->session->userdata('level') == 1) { ?>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Nama User</label>
                  <div class="col-sm-10">
                    <select name="nama" class="form-control" required>
                      <option value="" hidden selected disabled>Pilih nama</option>
                      <?php foreach ($user as $row) { ?>
                        <option value="<?php echo $row->user_id ?>"><?php echo $row->nama ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          <?php } else { ?>
            <input type="hidden" class="form-control" name="nama" value="<?php echo $this->session->userdata('user_id') ?>" />
          <?php } ?>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">dari</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" name="date_from" required />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">sampai</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" name="date_to" value="<?php echo date('Y-m-d');?>" required />
                </div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-gradient-primary mr-2">Print</button>
        </form>
      </div>
    </div>
  </div>
</div>