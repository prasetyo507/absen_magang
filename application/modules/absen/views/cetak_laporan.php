<?php
/*  echo '<pre>';
		print_r($lognya);  */
//ob_start();

$pdf = new FPDF("P","mm", "A4");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetMargins(10, 0 ,0);
$pdf->SetFont('Arial','B',14);

$pdf->image(site_url().'unggah/logo/pti.png',12,8,15);
$pdf->setXY(30,8);
$pdf->Cell(200,5,'Direktorat Pengembangan Teknologi Informasi',0,1,'L');
$pdf->setXY(30,13);
$pdf->SetFont('Arial','B',12); 
$pdf->Cell(200,5,'Jl. Raya perjuangan Marga Mulya Bekasi Utara, Kota Bekasi',0,1,'L');
$pdf->setXY(30,18);
$pdf->Cell(200,5,'Tlp. (021)8895-5882, Fax. (021)8895-5871',0,1,'L');
$pdf->Line(10,28,200,28);

$pdf->setXY(10,40);
$pdf->Cell(200,5,'Laporan Absensi Magang Dit.PTI',0,1,'C');
$pdf->ln();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(10,9,'No',1,0,'C');
$pdf->Cell(55,9,'User',1,0,'C');
$pdf->Cell(55,9,'Log',1,0,'C');
$pdf->Cell(20,9,'Status',1,0,'C');
$pdf->Cell(50,9,'Selisih',1,0,'C');
$total = 0;
$no=1; foreach ($lognya as $row) { 
	
	$pdf->ln();
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(10,9,$no,1,0,'C');
	$pdf->Cell(55,9,getName($row->user),1,0,'L');
	$pdf->Cell(55,9,datetimeIdn($row->log),1,0,'R');
	$pdf->Cell(20,9,getStatus($row->status),1,0,'L');
	$pdf->Cell(50,9,time_ago($row->selisih),1,0,'L');
	$total += $row->selisih;
	$no++; } 	
	$pdf->ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(140,5,'Total',1,0,'R');
	$pdf->Cell(50,5,time_ago($total),1,1,'L');

//exit();

$pdf->Output('laporan'.date('ymd_his').'.PDF','I');

?>

