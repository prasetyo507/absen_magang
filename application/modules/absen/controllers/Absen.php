<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absen extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('user_id') == null) {
			$this->session->set_flashdata('error', 'Maaf, Silahkan Login terlebih dahulu');
			redirect('auth');
		}
	}

	public function index()
	{
		$this->load->model('absen/model_absen');
		$data['user'] = $this->model_absen->user_magang()->result();
		$data['absennya'] = $this->model_absen->ambil($this->session->userdata('user_id'))->result();
		$data['page'] = 'absen/v_absen';
		$this->load->view('template/template', $data);
	}

	public function tambah()
	{
		$this->load->model('absen/model_absen');
		$data['cek'] = $this->model_absen->cekAbsen($this->session->userdata('user_id'))->row();
		$data['page'] = 'absen/v_add_absen';
		$this->load->view('template/template', $data);
	}
	public function add()
	{
		$this->load->model('absen/model_absen');
		$cek = $this->model_absen->cekAbsen($this->session->userdata('user_id'))->row();
		if (!empty($cek)) {
			if ($cek->status == 1) {
				$status = 2;
				$diff = strtotime(date('Y-m-d H:i:s')) - strtotime($cek->log);
			}
			if ($cek->status == 2) {
				$status = 1;
				$diff = 0;
			}
		} else {
			$status = 1;
			$diff = 0;
		}
		$data = array(
			'user'		=> $this->session->userdata('user_id'),
			'log'		=> date('Y-m-d H:i:s'),
			'status'	=> $status,
			'selisih'	=> $diff
		);
		$this->model_crud->insertdata('tbl_log', $data);
		echo "<script>alert('Terima kasih');
	document.location.href='" . base_url() . "absen';</script>";
	}
	public function print()
	{
		$this->load->library('Cfpdf');
		$this->load->model('model_absen');
		$nama = $this->input->post('nama');
		$date1 = $this->input->post('date_from');
		$date2 = $this->input->post('date_to');

		$result = $this->model_absen->print_report($date1, $date2, $nama);
		/* echo "<pre>";
		print_r($result);
		exit; */
		$data['lognya'] = $result;
		$this->load->view('cetak_laporan', $data);
	}
}
