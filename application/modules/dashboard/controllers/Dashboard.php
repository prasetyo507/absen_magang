<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('user_id') == null) {
			$this->session->set_flashdata('error', 'Maaf, Silahkan Login terlebih dahulu');
			redirect('auth');
		}
	}
	public function index()
	{
		$data['page'] = 'dashboard/v_dashboard';
		$this->load->view('template/template',$data);
			
	}
}
	
